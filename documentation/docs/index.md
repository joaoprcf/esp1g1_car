

# Welcome to ES Docs

#Client Side

## Description of concepts

* In this project our objective is implement Full  ELK or similar  stack for system monitoring. Full working system and a simple interface to control a car, that can be used by a simple men without to much formation, in natural disasters.

##Features

* Used to replace humans in natural disasters, to  reconnaissance the field for example , earthquakes, tsunamis and other disasters.


##Architecture

<img src="images/architecture1.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" />

##Technical Requirements

* SBrick car, MySQL (database), SpringBoot, Kafka, Bluetooth, Raspberry Pi,View js


#Schedule of features

##Milestone 1

* Define a system overall rationale , Sample project on docker , Maven, broker.

##Milestone 2

* First  deployment of system with minimum features, Identify scenarios and features and schedule them across iterations.

##Milestone 3

* Ensure that system is valuable for the user, and that user can use the interface easily, Web UI dashboard, Basic flow of processing events / streams ( e.g. alarms, converter ) and UI , Draft REST API for data access and other identified services ( may depend on project ).


##Milestone 4

* System support to historic data and UI for review , (pending) Present basic code analytics on projects.

##Milestone 5

* Basic acceptance testing using BDD ( coverage may depend on project ) , Automated code delivery i.e. no more IDE to deploy project ( can be maven evoked from IDE...), Deploy system wide logging - Logging at components levels ( logstash? Messaging? ).

##Milestone 6

* Expect full system Continuous deployment using Jenkins.

##Milestone 7

* Full  ELK or similar  stack for system monitoring ,Full working system.

#Tecnologies

##Vue.js

* GUI interface for managing the cars, it allows the user to control the car.



##SpringBoot.js

* Spring boot is responsible to the database managament and is the brain behind the car control, it keeps track of the overall latency between the user and the car.

##Raspberry Pi

* This device will relay commands from the springboot, driving is done by sending specific messages with an specific structure and is sent to the SBrick through Bluetooth LE. Latency between this two points has become a challenge, we were able to control the car using the gatttool unix command.

##SBrick

* Device that will be controlled via bluetooth, this device can move forward, backward, and has some flexibily to the sides.

#Personas/Users

##Users - Fireman

* Jhonn Dexter, 43 I am a born and raised Alabama boy, and like any good country boy I clean up good and know how to turn on the charm. I love spending the weekend outside exploring the area, BBQing with friends, and harassing my dog. (He secretly loves it, of course.) I will warn you that I dance like a fool at weddings. Really. I will embarrass you. But if I do my job right, you’ll be laughing too much to care. I am a fireman a care about other person, I life for another life.

<img src="images/fireman.jpg"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" />

## User - Army

*   Peter Parker, 33 I’m a fun-loving guy who’s a happy dog-dad to my girl Roxie. My friends would probably describe me as goofy but somehow I always end up being the responsible one. I have a lot of hobbies to keep up with. At the moment I’m focused on softball and fishing. One helps me get out and be social and the other helps me get away from it all. If you don’t mind the pup or a little bit of a goof we could be a pretty good pair. I've been in the military for over 10 years. I've been to Afghanistan 3 times over the past 10 years and I`ve learned a lot from these trips.

<img src="images/armyman.jpg"
     alt="Markdown Monster icon"
     style="float: center; margin-right: 10px;" />

#Scenarios

##Earthquakes

A 7.8-magnitude earthquake hit Baguio on July 16, 2018, causing total destruction, where buildings were all at risk of collapse and no more human lives could be put at stake, it is necessary to use robots to assess the condition of buildings.

#Client

##Architecture / Developer area 


##Communication between SBrick and Raspberry Pi


<img src="images/car_rasp.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" />


The lego car consists of a raspberry pi 3, functioning as the central control point of the car, and a sbrick device for receiving the commands required by the raspberry pi performing the requested actions on the engines, consequently shifting the car from a format to specify the intensity and direction of rotation of the motor. An ultrasonic sensor was used in order to identify obstacles and stop the driving of the car when an obstacle was detected


These control messages are sent by Bluetooth Low Energy (BLE) and the format is as follows:

|  Engine specification  |      Engine direction (Clockwise)      |   Intensity of rotation    |
| -------- |:-------------:| ---------:|
| 8 bits | 8 bits | 8 bits |



* To send messages to sbrick we had to use the command gatttool, a tool for sending and receiving packets to BLE. 

* It was not possible to keep a session open, that is, a pairing between the raspberry pi and the sbrick is established when the packet is sent and after receiving the packet is unpaired between raspberry and sbrick. 

* This leads to an increase in latency between sending and receiving packets sent between them. 

* If packets are sent consecutively with a minimum time gap, this pairing remains. 

* In order to reduce the latency between them a script was built in expect which consists of sending constant commands to sbrick so as not to lose the interconnection between them and packet send latency is minimal. 

* If there is no command requested by the user, the program in expect will send commands of the stop type of the car's motors.


<b>Communication between Raspberry Pi and Spring Boot</b>

<img src="images/rasp_spring.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" />

Car Registration:
At the moment the raspberry pi has connectivity to the internet it verifies if they have connectivity with the machine that contains the kafka broker. If there is a connection, the car sends a message to the broker kafka. 

Format of messages exchanged:

* {	“type” : 0,
	“mac_addr”: <mac address do carro>
}
This message is sent to the topic: "registerserver".

After receiving this message on the Spring Boot server, it checks the MySql database built with JPA feature if the car with a given MAC Address is found registered in the database.


* If registered in the database, the spring server informs the raspberry of the cart about the assigned id in order to subscribe to the topic that corresponds to it, otherwise a new id is generated and the same way it is informed from a message sent to the broker kafka about the assigned id.

This message is sent to the 'registerclient' topic and the message format is as follows:

* {	“type” : 1,
	“mac_addr”: <mac address do carro>,
	“Cid”: <id do carro>
}


* The raspberry pi when receiving this type of message from the subscription of the Kafka topic mentioned above, compares the MAC address and verifies if it coincides with itself. 

* If it matches, it registers the id and subscribes to the topic "commandclient <car id>".
This topic will insert the cart control messages generated from the Spring Boot server. 

* Because the number of control messages is high, and so as not to require high processing at the raspberry pi that is used by the car, it was decided to create a dedicated topic for the commands for each car that registers to the system.

The format of the command messages is as follows:

* 
{

	“type”:2,

	“cmd_type”:1,

	“msg_id”: <id único da mensagem para o carro>

	“cid”: <car id>,

	“left_pw”: <intensidade do motor da roda esquerda do carro [0-100]>,

	“right_pw”: <intensidade do motor da roda direita do carro [0-100]>,

	“left_cw”: <sentido de rotação da roda esquerda. 1-clockwise; 0-not clockwise>

    “right_cw”: <sentido de rotação da roda direita. 1-clockwise; 0-not clockwise>

}

Another type of command message is as follows:

* {  	
    “type”: 4

    “cid”: <car di>

    “cmd”: <command to send to the sbrick>

}

* In this last type of message the processing and construction of the command to send to the sbrick is done on the server side so as not to require processing the raspberry pi in order to interpret and construct by itself the command message to send to sbrick. 

* This last message is the one chosen for the control of the car.

* Upon receiving and executing a command directed to the car, raspberry pi sends a message to the kafka in order to calculate the latency between sending the message and executing the command for monitoring purposes from the website. 

* Raspberry generates a latency type message and inserts the current date and time and sends it to the "latencyserver" topic. 

* The message id will be equivalent to the id of the control message received, this in order for the server to distinguish the message and calculate and associate the latency of the message.


The format of the message is as follows:


* {	
    “type”:3,

	“msg_id”: <id da mensagem>,

	“cid”: <id do carro>,

	“date”: <data e tempo atual>

}

* After receiving this message from the spring boot server the message is associated with the id and the latency is calculated. After the latency calculation is performed, the latency information is sent to the Vue so as to make it available on a chart presented on the dashboard.



<b>Communication between Spring Boot - Kafka - MySql database</b>


* In the spring boot we have integration with kafka and the mysql database
basically all messages exchanged with the car either for sending or reply are recorded in the dataBase, including the car number, the message type and the command
all tables in the database have enpoints for the data query
all logs generated by spring are also sent to kafka and for this we use log4j because that way we can see the this in kibana.

<b>Spring endpoinsts for car:</b>

/ cars / all -> list all registered cars

/ cars / {id} -> allows you to search for a car for a given id (just change {id} to the id of the car, for example if you want the car registered with id 5 get / cars / 5


/ cars / mac_address / {mac_address} -> allows you to search a car for mac_address (example if you want the car with the mac address 00: AF: 48: 67: BE: 04 the endpoint should be / cars / mac_address / 00: 48: 67: BE: 04


<b>Endpoint to view the orders placed ((messages sent to the car via the frontend))</b>

+ / commands / requests / all to see all requests made

* / commands / responses / all that shows all the answers given to each request made


The id of the response is recorded in the dataBase according to the id of the request to which this response is intended, so it is possible to relate requests and responses.


Basically if a request is registered with id 5 the response to that request will be registered with id 5 also and so you know that a particular request had a certain response.


<b>Finally we have an endpoint that is / writeCommand</b>

which is where we received the requests made by the interface of the executioner, the registrations in the bd and the sending to the car by kafka.


The database and message entities are converted from json to java objects to facilitate manipulation in the code.
we use the labproject client to test the spring logs that are sent to the kafka.

we also do some simple spring testing, basically insert data into the database and check the response of the endpoints and then delete the data from the dataBase again.

##REST API

All data can be queried by a REST API

* / cars / all - Listings of cars

* / cars / <id> - search for a car with id <id>

* / cars / mac_address / <mac_address> - search for a car from your mac address

* / commands / responses / all - shows all the answers given to the request layer made.

* / commands / responses / all shows the answers given to each request

##Pipeline


<img src="images/pipeline.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" />



For this project, a pipeline named p1g1_legocar was generated on the DevOPs machine. This is the main repository of the project. With a period of one hour this updates the contents that are in the repository (ie pulls the repository) and executes the pipeline. If the repository is updated, the jenkins pipeline is configured to update the contents and execute the pipeline.
The pipeline process specifications are in the file jenkins file built into the project repository.

The process of constructing and validating (testing) the project is as follows:

* First is done and started the process of the controller service done in vue from the command docker-compose build, in order to generate the controller's docker image, using this image are performed the graphical interface resiliency tests from the Jest technology , if these tests fail the entire pipeline process fails. The contents of the controller's docker-compose file contain the necessary dependencies for performing this. 

* If the tests are successfully validated, it is not necessary to re-execute the 'build' command for production, because in the same image we can run tests and put them into production, reducing the compilation time by half.

* After it has been built and tested the graphic interface component of the system is tagged from the image to allow the consequent docker push of this image to the machine where the docker registry is located.

* The next phase of the pipeline is to start the Spring Boot server service process, where the services to which our server is dependent, for example MySQL, are arranged. 

* This is done from the junit technology in order to check if the project is in a state suitable for deployment. In turn the project binaries are placed on the repository machine, specifically in the artifactory repository named p1g1. 

* Sending project binaries is done from the API using curl to update these binaries in the repository. The generated docker image is tagged from this and also sent to the docker registry repository from docker push.

* Since the machine where the pipeline runs is on the DevOps machine and not the Run time it is decided to remove the generated images on the machine devops.

* And finally a signal is sent from an http request to a bash server that is on the Runtime machine so that it updates the project and starts the service from our system if it is stable and tested by the Jenkins pipeline created and configured .




##ELK


<img src="images/elk.jpg"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" />


* For our system we decided to have our own ELK Stash by implementing an official dock image of ELK Stash on a local computer. 

* We use Kafk to send the logs to store in logstash. The logs are structured by spring boot from the log4j2 technology. 

* The topic for which the logs generated by the spring boot of our service are sent is "p1g1_log". Locally we use logstash to collect all this information from the mentioned topic and graphically display all this information from Kibana using Elasticsearch queries. 

* All the logging information for logs is the logs generated by the Spring boot as well as the messages that are sent between the car and the server.