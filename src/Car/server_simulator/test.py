import json
import sys
from kafka.admin import KafkaAdminClient, NewTopic
from kafka import KafkaConsumer, KafkaProducer
from threading import Thread
import os

REGISTER_SERVER = 'registerserver'
REGISTER_CLIENT = 'registerclient'
REGISTER		= 0
REGISTER_ACK	= 1
ID = 0
counter = 0

def create_topic(topic, local="localhost:9092"):
	admin_client = KafkaAdminClient(bootstrap_servers="localhost:9092")
	topic_list = []
	topic_list.append(NewTopic(name=topic, num_partitions=1, replication_factor=1))
	admin_client.create_topics(new_topics=topic_list, validate_only=False)
	print("Created topic ",topic,".")

def send_to_topic(topic, pkt, local="localhost:9092"):
	producer 	= KafkaProducer(bootstrap_servers='localhost:9092')
	producer.send(topic,str.encode(pkt))
	producer.flush()	
	print('Pkt sent to topic ',topic,'.')

def listen_latency():
	consumer    = KafkaConsumer('latencyserver', auto_offset_reset='earliest',
                                    bootstrap_servers=['localhost:9092'],
                                    enable_auto_commit=True, 
                                    api_version=(0, 10), consumer_timeout_ms=1000)	
	while(True):
		for msg in consumer:
			tmp = json.loads(msg.value.decode('utf-8'))
			print('Pkt received:\t',tmp)
			if(tmp['type'] != 3):
				print('Pkt is not type LATENCY.')
				continue
			print('Msg Id:\t',tmp['msg_id'])
			print('CID:\t',tmp['cid'])
			print('Time:\t',tmp['time'])

	consumer.close()

def create_command_pkt(command, cid, msg_id):
	pkt = dict()
	pkt['type'] 	= 2 #COMMAND
	pkt['cid']		= cid
	pkt['msg_id']	= msg_id
	pkt['cmd_type'] = 1
	pkt['left_pw'] 	= 100
	pkt['right_pw']	= 100
	#Front
	if(command == 'f'):
		pkt['left_cw']	= True
		pkt['right_cw']	= False
	#Back
	elif(command == 'b'):	
		pkt['left_cw']	= True
		pkt['right_cw']	= False
	#Stop
	else:
		pkt['cmd_type'] = 0
	pkt = json.dumps(pkt)
	print('Pkt command constructed:\t',pkt)
	return pkt

if __name__ == '__main__':
	##Clear topics
	os.system('sudo ./delete-topics.sh')
	create_topic('registerserver')
	create_topic('registerclient')
	
	consumer    = KafkaConsumer('registerserver', auto_offset_reset='earliest',
                                    bootstrap_servers=['localhost:9092'],
                                    enable_auto_commit=True, 
                                    api_version=(0, 10), consumer_timeout_ms=1000)	
	tmp_topic 	= None
	tmp_cid		= None
	while(tmp_cid == None):
		for msg in consumer:
			tmp = json.loads(msg.value.decode('utf-8'))
			print("Message received:\t",tmp)
			if( not tmp['type'] == REGISTER):
				print('Pkt not type of REGISTER.')
				continue
			mac_addr = tmp['mac_addr']
			pkt = dict()
			pkt['type']		= REGISTER_ACK
			pkt['mac_addr'] = mac_addr
			pkt['cid']		= ID
			tmp_topic 	= 'commandclient'+str(ID)	
			tmp_cid		= ID
			create_topic(tmp_topic)
			ID = ID +1
			pkt = json.dumps(pkt)
			print("Created REGISTER_ACK pkt:\t",pkt)
			send_to_topic('registerclient',pkt)
			print("Sent REGISTER_ACK pkt.")
			break

	print("Server listening to latency")
	t = Thread(target= listen_latency, args=())
	t.start()

	##Send command	
	consumer.close()
	while(True):
		command = input('Command (f,b,s):\t')
		tmp = None
	
		if command in ['f','b','s']:
			tmp = create_command_pkt(command,tmp_cid, counter)
			counter = counter + 1
			send_to_topic(tmp_topic,tmp)
			print('Pck COMMAND sent.')
		
		else:
			print('command does not exist')
	t.join()	
