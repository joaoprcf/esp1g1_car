import car_controller
#from msg_handler import *
import re, uuid
import json
import packet
import topics
#import _thread
from threading import Thread
import time
from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import KafkaError
from datetime import datetime
import sys
from bluetooth import *


##Global variables
CID	= None


def find_devices():
	devices = discover_devices(lookup_names = True)
	for name, addr in devices:
		print(str(name),'-',str(addr))

##Send latency response
def send_latency_pkt(msg_id):
	pkt = dict()
	pkt['type'] 	= packet.LATENCY
	pkt['msg_id']	= msg_id
	pkt['cid']		= CID
	datetmp			= datetime.now()
	pkt['time']		= ('%02d:%02d.%d'%(datetmp.minute, datetmp.second, datetmp.microsecond))[:-4]
	pkt = json.dumps(pkt)
	print("Constructed latency pkt:\t",pkt)	
	send_kafka_pkt(pkt, topics.LATENCY_SERVER, boot_server='192.168.2.1:9092')


##Command
def car_command():
	global CID
	tmp_topic = topics.COMMAND_CLIENT+str(CID)
	print('Subscribing to topic:\t'+tmp_topic)
	consumer 	= KafkaConsumer(	tmp_topic, auto_offset_reset='earliest',
									bootstrap_servers=['192.168.2.1:9092'],
									enable_auto_commit=True, 
									api_version=(0, 10), consumer_timeout_ms=1000)	
	while(True):
		for msg in consumer:
			tmp = json.loads(msg.value.decode('utf-8'))
			if(not tmp['type'] == packet.COMMAND):
				print('Pkt not ype of COMMAND')		
				continue
			if(tmp['cid'] == CID):	
				#Stop car
				if(tmp['cmd_type'] == 0):
					print('Command type 0 received.')
					car_controller.stop_car()

				#2 up/low joystick
				elif(tmp['cmd_type'] == 1):
					print('Command type 1 received.')
					car_controller.mv_wheel(tmp['left_pw'], True, tmp['left_cw'])	
					car_controller.mv_wheel(tmp['right_pw'], False, tmp['right_cw'])	
			
				#1 joystick
				elif(tmp['cmd_type'] == 2):
					print('Command type 2 received.')
					pass
				#Unkown command
				else:
					print("Command Type Unknown")
					continue
				
				#Send latency packer
				send_latency_pkt(tmp['msg_id'])
	consumer.close()

##Wait for registration & ID attribution
def registration():
	global CID
	mac_addr 	= get_btmac()
	consumer 	= KafkaConsumer(	topics.REGISTER_CLIENT, auto_offset_reset='earliest',
									bootstrap_servers=['192.168.2.1:9092'],
									enable_auto_commit=True, 
									api_version=(0, 10), consumer_timeout_ms=1000)	
	done	= False
	send_kafka_pkt(register_pkt(),topics.REGISTER_SERVER,boot_server='192.168.2.1:9092')
	while(not done):
		for msg in consumer:
			print('msg:\t',msg)
			tmp = json.loads(msg.value.decode('utf-8'))
			print('Packet received:\t',tmp)
			if( not tmp['type'] == packet.REGISTER_ACK):
				print('Pkt not type of REGISTER_ACK.')
				continue

			if(tmp['mac_addr'] == mac_addr):
				CID = tmp['cid']	
				print("Got new ID:\t",CID)
				done = True
				break
		time.sleep(10)
		send_kafka_pkt(register_pkt(),topics.REGISTER_SERVER,boot_server='192.168.2.1:9092')
	consumer.close()

#Send pkt trhough kafka
def send_kafka_pkt(pkt, topic, boot_server='192.168.2.1:9092'):
	producer	= KafkaProducer(bootstrap_servers='192.168.2.1:9092')
	producer.send(topic,str.encode(pkt))
	producer.flush()
	print('Paket sent to Kafka broker.')



##Get Bluetooth device MAC Address
def get_btmac():
	mac_address = ':'.join(re.findall('..', '%012x' % uuid.getnode()))	
	return mac_address

##Create REGISTER pkt.
def register_pkt():
	pkt = dict()
	pkt['mac_addr'] = get_btmac()
	pkt['type'] 	= packet.REGISTER
	pkt = json.dumps(pkt)
	print('REGISTER pkt constructed:\t',pkt) 
	return pkt

if __name__ == '__main__':
	#Registration
	try:
		t = Thread(target = registration, args=())
		t.start()
		t.join()
	except:
		print("Error: unable to handle registration thread")
	print("Register procedure done.")	
	print('Preparing car command.')
	try:
		t = Thread(target = car_command, args=())
		t.start()
		t.join()
	except:
		print("Error: Unable to handle car_command thread.")
		sys.exit(1)
