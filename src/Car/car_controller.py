import os

##Constants
COMMAND 			= 'sudo gatttool -b '
BT_INT	 			= '-i hci0 '
CHAR_WRITE 			= '--char-write '
CHAR_WRITE_REQUEST 	= '--char-write-req '
HANDLE				= '--handle=0x1A '
VALUE				= '--value='
SPACE				= ' '
LEFT_WHEEL			= "0103"
RIGHT_WHEEL			= "0101"
CLOCKWISE			= "00"
NOT_CLOCKWISE		= "01"

MAC_DEFAULT			= "00:07:80:D0:52:C6"

# Regular SBRICK command
#	@param mac string BT mac of SBrick
#	@return regural command to controll the car
def predefined_command(mac):
	return COMMAND + mac + SPACE + BT_INT + CHAR_WRITE_REQUEST + HANDLE + VALUE

# Go front 
#	@param mac string BT mac of SBrick
#	@param integer motor's power (0 -100) 
def go_front(power, mac=MAC_DEFAULT):
	print(predefined_command(mac) + LEFT_WHEEL + CLOCKWISE + calc_power(power))
	os.system(predefined_command(mac) + LEFT_WHEEL + CLOCKWISE + calc_power(power))
	os.system(predefined_command(mac) + RIGHT_WHEEL + NOT_CLOCKWISE + calc_power(power))
	
# Go back
#	@param mac string BT mac of SBrick
#	@param integer motor's power (0 -100)
def go_back(power, mac=MAC_DEFAULT):
	os.system(predefined_command(mac) + LEFT_WHEEL + NOT_CLOCKWISE + calc_power(power))
	os.system(predefined_command(mac) + RIGHT_WHEEL + CLOCKWISE + calc_power(power))

# Go left
#	@param mac string BT mac of SBrick
#	@param integer motor's power (0 -100)
def go_left(power, mac=MAC_DEFAULT):
	os.system(predefined_command(mac) + LEFT_WHEEL + CLOCKWISE  
					+ calc_power((8/10) * power))
	os.system(predefined_command(mac) + RIGHT_WHEEL + NOT_CLOCKWISE 
					+ calc_power(power))

# Go right
#	@param mac string BT mac of SBrick
#	@param integer motor's power (0 -100)
def go_right(power, mac=MAC_DEFAULT):
	os.system(predefined_command(mac) + LEFT_WHEEL + CLOCKWISE  
					+ calc_power(power))
	os.system(predefined_command(mac) + RIGHT_WHEEL + NOT_CLOCKWISE  
					+ calc_power((8/10) * power))

# Stop car
#	@param mac string BT mac of SBrick
def stop_car(mac=MAC_DEFAULT):
	os.system(predefined_command(mac) + LEFT_WHEEL + "0000")
	print(predefined_command(mac) + LEFT_WHEEL + "00")
	os.system(predefined_command(mac) + RIGHT_WHEEL + "0000")

# Moves one wheel for the Command Type 1
#	@param intensity of car's wheel rotation.
#	@param lw move left_wheel flag. If true, moves left wheel, otherwise moves right wheel
#	@param clockwise move car's wheel in clockwise if set true. No clockwise otherwise.
#	@param mac string BT mac of SBrick
def mv_wheel(power,lw=True,clockwise=True, mac=MAC_DEFAULT):
	print('Command:\t',predefined_command(mac) + (LEFT_WHEEL if lw else RIGHT_WHEEL) + 
				(CLOCKWISE if clockwise else NOT_CLOCKWISE) + calc_power(power))
	os.system(predefined_command(mac) + (LEFT_WHEEL if lw else RIGHT_WHEEL) + 
				(CLOCKWISE if clockwise else NOT_CLOCKWISE) + calc_power(power))


# Calculate power
#	@param power (0-100)% power
#	@return hexadecimal value to concatenate value command 
def calc_power(power):
	assert power >= 0 and power <= 100
	dec = int((255 * power) / 100)
	return str(hex(dec).split('x')[-1]).upper()

if __name__ == '__main__':
	go_front(100)
	stop_car()
