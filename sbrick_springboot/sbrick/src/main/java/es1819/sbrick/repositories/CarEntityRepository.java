package es1819.sbrick.repositories;


import es1819.sbrick.entities.persistence.CarEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CarEntityRepository extends CrudRepository<CarEntity, Long> {

    //Queries goes in here
    @Query(value = "select * from cars where MacAddress = :#{#macAddress} ", nativeQuery = true) //TODO: dont sure if it will works
    Optional<CarEntity> findByMacAddress(@Param("macAddress") String macAddress);
}
