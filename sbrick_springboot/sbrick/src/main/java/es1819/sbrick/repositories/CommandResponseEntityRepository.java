package es1819.sbrick.repositories;

import es1819.sbrick.entities.persistence.CommandResponseEntity;
import es1819.sbrick.entities.persistence.ResponseEntity;
import org.springframework.data.repository.CrudRepository;

public interface CommandResponseEntityRepository extends CrudRepository<CommandResponseEntity, Long> {
    //Queries goes in here

}
