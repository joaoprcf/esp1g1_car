package es1819.sbrick.repositories;

import es1819.sbrick.entities.persistence.ResponseEntity;
import org.springframework.data.repository.CrudRepository;

public interface ResponseEntityRepository extends CrudRepository<ResponseEntity, Integer> {
    //Queries goes here
}
