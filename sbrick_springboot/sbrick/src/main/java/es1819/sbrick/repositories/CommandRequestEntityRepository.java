package es1819.sbrick.repositories;

import es1819.sbrick.entities.persistence.CommandRequestEntity;
import org.springframework.data.repository.CrudRepository;

public interface CommandRequestEntityRepository extends CrudRepository<CommandRequestEntity, Long> {
    //Queries goes here
}
