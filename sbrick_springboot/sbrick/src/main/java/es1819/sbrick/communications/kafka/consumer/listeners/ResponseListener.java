package es1819.sbrick.communications.kafka.consumer.listeners;

import es1819.sbrick.entities.persistence.ResponseEntity;
import es1819.sbrick.repositories.ResponseEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class ResponseListener {

    private static final Logger logger = LoggerFactory.getLogger(ResponseListener.class);

    @Autowired
    ResponseEntityRepository responseEntityRepository;

    @KafkaListener(topics = "${sbrick.consumer.response.topic}",
            containerFactory = "kafkaResponseListenerContainerFactory")
    public void consume(ResponseEntity responseEntity) {
        logger.info("payload = " + responseEntity.toString());

        responseEntityRepository.save(responseEntity);
    }
}
