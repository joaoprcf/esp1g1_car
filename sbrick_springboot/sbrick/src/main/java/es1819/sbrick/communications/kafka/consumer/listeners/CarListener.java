package es1819.sbrick.communications.kafka.consumer.listeners;

import es1819.sbrick.entities.rest.CarRegistrationResponse;
import es1819.sbrick.entities.persistence.CarEntity;
import es1819.sbrick.repositories.CarEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class CarListener {

    private static final Logger logger = LoggerFactory.getLogger(CarListener.class);

    @Autowired
    CarEntityRepository carEntityRepository;

    @Autowired
    KafkaTemplate<String, Object> kafkaTemplate;

    @KafkaListener(topics = "${sbrick.consumer.car.register.topic}",
            containerFactory = "kafkaCarListenerContainerFactory")
    public void consume(CarEntity carEntity) {
        logger.info("payload = " + carEntity.toString());

        Optional<CarEntity> registeredCar = carEntityRepository.findByMacAddress(carEntity.getMacAddress());
        if(registeredCar.isPresent())
            logger.info("Car already registered with id " + registeredCar.get().getId());
        else {
            CarEntity newCarEntity = new CarEntity();
            newCarEntity.setMacAddress(carEntity.getMacAddress());

            carEntityRepository.save(newCarEntity);
            logger.info("Car successfully registered with id " + newCarEntity.getId());

            carEntity = newCarEntity; //Update the variable reference
        }

        CarRegistrationResponse carRegistrationResponse =
                new CarRegistrationResponse(carEntity.getId(), 1, carEntity.getMacAddress());
        kafkaTemplate.send("registerclient", UUID.randomUUID().toString(), carRegistrationResponse);
    }
}
