package es1819.sbrick.communications.kafka.consumer.configurations;

import es1819.sbrick.entities.persistence.CarEntity;
import es1819.sbrick.entities.persistence.ResponseEntity;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class ConsumersConfiguration {

    @Autowired
    private KafkaProperties kafkaProperties;

    //Default kafka consumer
    @Bean
    public ConsumerFactory<String, String> kafkaConsumerFactory() {
        Map<String, Object> properties = new HashMap<>(kafkaProperties.buildConsumerProperties());

        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(
                properties, new StringDeserializer(), new StringDeserializer());
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();

        factory.setConsumerFactory(kafkaConsumerFactory());
        return factory;
    }

    //Car kafka consumer
    @Bean
    public ConsumerFactory<String, CarEntity> kafkaCarConsumerFactory() {
        Map<String, Object> properties = new HashMap<>(kafkaProperties.buildConsumerProperties());

        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(
                properties, new StringDeserializer(), new JsonDeserializer<>(CarEntity.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, CarEntity> kafkaCarListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, CarEntity> factory =
                new ConcurrentKafkaListenerContainerFactory<>();

        factory.setConsumerFactory(kafkaCarConsumerFactory());
        return factory;
    }

    //Response kafka consumer
    @Bean
    public ConsumerFactory<String, ResponseEntity> kafkaResponseConsumerFactory() {
        Map<String, Object> properties = new HashMap<>(kafkaProperties.buildConsumerProperties());

        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(
                properties, new StringDeserializer(), new JsonDeserializer<>(ResponseEntity.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, ResponseEntity> kafkaResponseListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, ResponseEntity> factory =
                new ConcurrentKafkaListenerContainerFactory<>();

        factory.setConsumerFactory(kafkaResponseConsumerFactory());
        return factory;
    }
}
