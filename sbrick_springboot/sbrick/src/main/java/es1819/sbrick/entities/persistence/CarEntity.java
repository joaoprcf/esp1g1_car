package es1819.sbrick.entities.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cars", schema = "sbrick", catalog = "")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarEntity {

    private long id;
    private String macAddress;

    public CarEntity() {
        //empty constructor
    }

    public CarEntity(long id, String macAddress) {
        this.id = id;
        this.macAddress = macAddress;
    }

    @Id
    @Column(name = "Id")
    @JsonProperty("cid")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "MacAddress")
    @JsonProperty("mac_addr")
    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarEntity that = (CarEntity) o;
        return id == that.id &&
                Objects.equals(macAddress, that.macAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, macAddress);
    }

    @Override
    public String toString() {
        return "CarEntity{" +
                "id=" + id +
                ", macAddress='" + macAddress + '\'' +
                '}';
    }
}
