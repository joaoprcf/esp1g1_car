package es1819.sbrick.entities.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "command_responses", schema = "sbrick", catalog = "")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommandResponseEntity {
    private long id;
    private long carId;
    private int type;
    private String command;

    public CommandResponseEntity() {
        //Empty constructor
    }

    public CommandResponseEntity(long id, long carId, int type, String command) {
        this.id = id;
        this.carId = carId;
        this.type = type;
        this.command = command;
    }

    @Id
    @Column(name = "Id")
    @JsonProperty("msg_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CarId")
    @JsonProperty("cid")
    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    @Basic
    @Column(name = "Type")
    @JsonProperty("type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "Command")
    @JsonProperty("cmd")
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommandResponseEntity that = (CommandResponseEntity) o;
        return id == that.id &&
                carId == that.carId &&
                type == that.type &&
                Objects.equals(command, that.command);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, carId, type, command);
    }

    @Override
    public String toString() {
        return "CommandResponseEntity{" +
                "id=" + id +
                ", carId=" + carId +
                ", type=" + type +
                ", command='" + command + '\'' +
                '}';
    }
}
