package es1819.sbrick.entities.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "command_requests", schema = "sbrick", catalog = "")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommandRequestEntity {
    private long id;
    private String command;
    private Long date;

    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("msg_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Command", nullable = true, length = 255)
    @JsonProperty("cmd")
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Basic
    @Column(name = "Date", nullable = true)
    @JsonProperty("date")
    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommandRequestEntity that = (CommandRequestEntity) o;
        return id == that.id &&
                Objects.equals(command, that.command) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, command, date);
    }

    @Override
    public String toString() {
        return "CommandRequestEntity{" +
                "id=" + id +
                ", command='" + command + '\'' +
                ", date=" + date +
                '}';
    }
}
