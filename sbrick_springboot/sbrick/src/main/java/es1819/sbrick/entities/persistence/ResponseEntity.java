package es1819.sbrick.entities.persistence;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "responses", schema = "sbrick", catalog = "")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseEntity {

    private int requestId;
    private int messageType;
    private String time;

    @Id
    @Column(name = "RequestId")
    @JsonProperty("msg_id")
    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "MessageType")
    @JsonProperty("type")
    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    @Basic
    @Column(name = "Time")
    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseEntity that = (ResponseEntity) o;
        return requestId == that.requestId &&
                messageType == that.messageType &&
                time.equals(that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, messageType, time);
    }

    @Override
    public String toString() {
        return "ResponseEntity{" +
                "requestId=" + requestId +
                ", messageType=" + messageType +
                ", time='" + time + '\'' +
                '}';
    }
}
