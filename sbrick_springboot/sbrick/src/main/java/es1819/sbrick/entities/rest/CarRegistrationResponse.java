package es1819.sbrick.entities.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CarRegistrationResponse {

    private long carId;
    private int messageType;
    private String macAddress;

    public CarRegistrationResponse(long carId, int messageType, String macAddress) {
        this.carId = carId;
        this.messageType = messageType;
        this.macAddress = macAddress;
    }

    @JsonProperty("cid")
    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    @JsonProperty("type")
    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    @JsonProperty("mac_addr")
    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarRegistrationResponse that = (CarRegistrationResponse) o;
        return carId == that.carId &&
                messageType == that.messageType &&
                macAddress.equals(that.macAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carId, messageType, macAddress);
    }

    @Override
    public String toString() {
        return "CarRegistrationResponse{" +
                "carId=" + carId +
                ", messageType=" + messageType +
                ", macAddress='" + macAddress + '\'' +
                '}';
    }
}
