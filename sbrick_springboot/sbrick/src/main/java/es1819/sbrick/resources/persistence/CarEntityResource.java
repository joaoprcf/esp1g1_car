package es1819.sbrick.resources.persistence;

import es1819.sbrick.entities.persistence.CarEntity;
import es1819.sbrick.repositories.CarEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController //Uses spring mvc
@RequestMapping(value = "/cars") //Rest object address
public class CarEntityResource {

    private static final Logger logger = LoggerFactory.getLogger(CarEntityResource.class);

    @Autowired
    CarEntityRepository carEntityRepository;

    @CrossOrigin()
    @GetMapping(value = "/all")
    public Iterable<CarEntity> findAll() {
        logger.info("Access made to endpoint /cars/all");
        return carEntityRepository.findAll();
    }

    @CrossOrigin()
    @GetMapping("/id/{id}")
    public ResponseEntity<CarEntity> findById(@PathVariable(value = "id") Long id) {
        logger.info("Access made to endpoint /car/" + id);

        Optional<CarEntity> carEntity = carEntityRepository.findById(id);
        if(carEntity.isEmpty())
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(carEntity.get());
    }

    @CrossOrigin()
    @GetMapping("/mac_address/{mac_address}")
    public ResponseEntity<CarEntity> findByMacAddress(@PathVariable(value = "mac_address") String macAddress) {
        logger.info("Access made to endpoint /car/" + macAddress);

        Optional<CarEntity> carEntity = carEntityRepository.findByMacAddress(macAddress);
        if(carEntity.isEmpty())
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(carEntity.get());
    }
}
