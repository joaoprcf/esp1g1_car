package es1819.sbrick.resources.persistence;

import es1819.sbrick.entities.persistence.CommandRequestEntity;
import es1819.sbrick.repositories.CommandRequestEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/commands/request")
public class CommandRequestEntityResource {

    private static final Logger logger = LoggerFactory.getLogger(CommandRequestEntityResource.class);

    @Autowired
    CommandRequestEntityRepository commandRequestEntityRepository;

    @CrossOrigin()
    @GetMapping(value = "/all")
    public Iterable<CommandRequestEntity> findAll() {
        logger.info("Access made to endpoint /commands/request/all");
        return commandRequestEntityRepository.findAll();
    }


}
