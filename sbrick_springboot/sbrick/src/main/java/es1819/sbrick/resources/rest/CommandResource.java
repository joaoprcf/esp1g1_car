package es1819.sbrick.resources.rest;

import es1819.sbrick.entities.persistence.CommandRequestEntity;
import es1819.sbrick.entities.persistence.CommandResponseEntity;
import es1819.sbrick.repositories.CommandRequestEntityRepository;
import es1819.sbrick.repositories.CommandResponseEntityRepository;
import es1819.sbrick.resources.persistence.CarEntityResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController //Uses spring mvc
public class CommandResource {

    private static final Logger logger = LoggerFactory.getLogger(CarEntityResource.class);

    @Autowired
    KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    CommandRequestEntityRepository commandRequestEntityRepository;

    @Autowired
    CommandResponseEntityRepository commandResponseEntityRepository;

    @CrossOrigin
    @RequestMapping(value = "/writeCommand", method = RequestMethod.POST)
    public ResponseEntity<String> sendCommand(@RequestBody CommandRequestEntity commandRequestEntity) {
        logger.info("Access made to endpoint /writeCommand");
        logger.info(commandRequestEntity.toString());

        commandRequestEntityRepository.save(commandRequestEntity);

        //create the car command based on the command received
        CommandResponseEntity commandResponseEntity = new CommandResponseEntity(commandRequestEntity.getId(), 0, 2,
            commandRequestEntity.getCommand());
        commandResponseEntityRepository.save(commandResponseEntity);

        //send the command to the car
        kafkaTemplate.send("commandclient0", UUID.randomUUID().toString(), commandResponseEntity);

        logger.info("Command send to kafka topic: commandclient0");
        logger.info(commandResponseEntity.toString());
        return ResponseEntity.ok("");
    }
}
