package es1819.sbrick.resources.persistence;

import es1819.sbrick.entities.persistence.ResponseEntity;
import es1819.sbrick.repositories.ResponseEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "responses")
public class ResponseEntityResource {

    private static final Logger logger = LoggerFactory.getLogger(ResponseEntityResource.class);

    @Autowired
    ResponseEntityRepository responseEntityRepository;

    @CrossOrigin()
    @GetMapping(value = "/all")
    public Iterable<ResponseEntity> findAll() {
        logger.info("Access made to endpoint /responses/all");
        return responseEntityRepository.findAll();
    }

}
