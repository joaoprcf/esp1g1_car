package es1819.sbrick.resources.persistence;

import es1819.sbrick.entities.persistence.CommandResponseEntity;
import es1819.sbrick.repositories.CommandResponseEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/commands/responses")
public class CommandResponseEntityResource {

    private static final Logger logger = LoggerFactory.getLogger(CommandResponseEntityResource.class);

    @Autowired
    CommandResponseEntityRepository commandResponseEntityRepository;

    @CrossOrigin()
    @GetMapping(value = "/all")
    public Iterable<CommandResponseEntity> findAll() {
        logger.info("Access made to endpoint /command/responses/all");
        return commandResponseEntityRepository.findAll();
    }
}
