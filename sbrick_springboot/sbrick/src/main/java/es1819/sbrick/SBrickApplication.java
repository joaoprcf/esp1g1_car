package es1819.sbrick;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "es1819.sbrick.repositories")
public class SBrickApplication {

	public static void main(String[] args) {
		SpringApplication.run(SBrickApplication.class, args);
	}

}
