package es1819.sbrick.resources.persistence;

import es1819.sbrick.entities.persistence.CarEntity;
import es1819.sbrick.repositories.CarEntityRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CarEntityResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    CarEntityRepository carEntityRepository;

    @Before
    public void testPreparation() {
        carEntityRepository.save(new CarEntity(0, "a8:20:81:37:dd:5a"));
        carEntityRepository.save(new CarEntity(1, "a8:20:46:37:fd:5a"));
    }

    @Test
    public void findAll() throws Exception {
        MvcResult status = mockMvc.perform(MockMvcRequestBuilders.get("/cars/all")).andReturn();
        assertEquals(200, status.getResponse().getStatus());
    }

    @After
    public void testFinalization() {
        carEntityRepository.deleteAll();
    }
}