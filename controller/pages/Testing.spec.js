import { mount } from '@vue/test-utils'
import index from './index.vue'
import about from './about.vue'
import documentation from './documentation.vue'

describe('index.vue', () => {
    test('setup correctly', () => {
        expect(true).toBe(true)
    });
})

describe('about.vue', () => {
    test('team page shows up', () => {
        const wrapper = mount(about)
        //expect(wrapper.text())
       // wrapper.find('button').trigger('click')
       expect(wrapper.contains('div.grid__display')).toBe(true);
    });
})

describe('documentation.vue', () => {
    const wrapper = mount(documentation)
    it('renders all docs', () => {
        expect(wrapper.find('section').exists()).toBe(true);
        expect(wrapper.contains('section.documentation')).toBe(true);
    })
})