const express = require('express');
const consola = require('consola');
const util = require('util');
const axios = require('axios')

/* var kafka = require('kafka-node');
var HighLevelProducer = kafka.HighLevelProducer 
var client = new kafka.KafkaClient()
var producer = new HighLevelProducer(client)  
producer.createTopics(['t'], true, function (err, data) {});

 */
var exec = util.promisify(require('child_process').exec);
const { Nuxt, Builder } = require('nuxt')
const app = express()

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')
async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)
  
  const { host, port } = nuxt.options.server
  
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }
  
  // Give nuxt middleware to express
  app.use(nuxt.render)
  
  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
let marker = new Date()

app.use(express.json())
app.post("/writeCommand",async (req,res) => {
  
  res.send(req.body)
})

app.post("/walk",async (req,res) => {
  const {left, right } = req.body

  marker = new Date()
  var {stdout, stderr} = await exec(`printf "char-write-req 0x1A 010100${right}\nchar-write-req 0x1A 010301${left}" > server/commands.txt`)

  setTimeout(async () => {
    if(new Date() - marker>=1350) {
      var {stdout, stderr} = await exec(`printf "char-write-req 0x1A 01010000\nchar-write-req 0x1A 01030100" > server/commands.txt`)
      console.log('Stoping')
    } 
  }, 1400)
  res.send("OK")
  
})
app.post("/back",async (req,res) => {
  const {left, right } = req.body
  var {stdout, stderr} = await exec(`printf "char-write-req 0x1A 010101${right}\nchar-write-req 0x1A 010300${left}" > server/commands.txt`)

  marker = new Date()
  setTimeout(async () => {
    if(new Date() - marker>=1350) {
      var {stdout, stderr} = await exec(`printf "char-write-req 0x1A 01010100\nchar-write-req 0x1A 01030000" > server/commands.txt`)
      console.log('Stoping')
    } 
  }, 1400)
  
  res.send("OK")
})

app.post("/elasticSearch", async (req,res) => {
  try {
    const elastic = await axios({
      url: 'http://192.168.160.80:9200/p1g1/command',
      method: 'post',
      data: {
        "command" : "walk",
        "latency": req.body.latency,
        "date" : ""+new Date()
      }
    })
    res.send(elastic.data);
  } catch(ex) {
    console.error('Error', ex.message);
    
  }
})




start()
const a = {
  type: 4,
  cmd: 'char-write-req 0x1A 01010100\nchar-write-req 0x1A 01030000'
}