import { Line } from "vue-chartjs";

export default {
  extends: Line,
  props: {
    labels: Array,
    data: Array
  },
  mounted() {
    // Overwriting base render method with actual data.
    this.renderChart({
      labels: this.labels,
      datasets: [
        {
          label: "latency",
          backgroundColor: ["#339"],
          data: this.data
        }
      ]
    },{
      maintainAspectRatio:false,
      scales: {
        yAxes: [{
            display: true,
            ticks: {
                suggestedMin: Math.min(...this.data)/2,    // minimum will be 0, unless there is a lower value.
            }
        }]
      }
    });
  },
  watch: {
    data: function(val) {
      this.$data._chart.update()
    }
  }

};
